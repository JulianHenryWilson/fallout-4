# Copyright Copyright 2022-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "The Zap Gun - a makeshift laser weapon"
    DESC = "Adds a modular laser rifle, pistol, submachine gun, sniper, flamethrower or shotgun. Custom animations, sounds, models and textures. Injected into levelled lists.Includes 3 craftable Zap Gun turrets."
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/53998"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    IUSE = "esl no-ll reduced-ll"
    REQUIRED_USE = "?? ( no-ll reduced-ll )"
    NEXUS_SRC_URI = """
       https://nexusmods.com/fallout4/mods/53998?tab=files&file_id=217146 -> Zap_Gun-53998-1-4-1631891092.7z
       esl? (  https://nexusmods.com/fallout4/mods/53998?tab=files&file_id=217156 -> Zap_Gun_-_ESL_version-53998-1-4-1631905304.7z )
       no-ll? ( https://nexusmods.com/fallout4/mods/53998?tab=files&file_id=217159 -> Zap_Gun_-_No_levelled_list_integration_(ESL)-53998-1-4-1631908957.7z )
       reduced-ll? ( https://nexusmods.com/fallout4/mods/53998?tab=files&file_id=217557 -> Zap_Gun_-_Reduced_LL_spawn_patch_-_ESL_version_(ESL)-53998-1-41-1632389034.7z )
       reduced-ll? ( https://nexusmods.com/fallout4/mods/53998?tab=files&file_id=217755 -> Zap_Gun_-_Reduced_LL_spawn_patch_-_ESP_version_(ESL-ified_ESP)-53998-1-44-1632648905.7z )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Zapgun",
            S="Zap_Gun-53998-1-4-1631891092",
            PLUGINS=[File("Zapgun.esp")],
            REQUIRED_USE="!esl !no-ll",
        ),
        InstallDir(
            "Zapgun",
            S="Zap_Gun_-_ESL_version-53998-1-4-1631905304",
            PLUGINS=[File("Zapgun.esp")],
            REQUIRED_USE="esl !no-ll",
        ),
        InstallDir(
            "Zapgun",
            S="Zap_Gun_-_No_levelled_list_integration_(ESL)-53998-1-4-1631908957",
            PLUGINS=[File("Zapgun.esp")],
            REQUIRED_USE="no-ll",
        ),
        InstallDir(
            ".",
            S="Zap_Gun_-_Reduced_LL_spawn_patch_-_ESL_version_(ESL)-53998-1-41-1632389034",
            PLUGINS=[File("ZapGunReducedLL.esl")],
            REQUIRED_USE="esl reduced-ll",
        ),
        InstallDir(
            ".",
            S="Zap_Gun_-_Reduced_LL_spawn_patch_-_ESP_version_(ESL-ified_ESP)-53998-1-44-1632648905",
            PLUGINS=[File("ZapGunReducedLL.esp")],
            REQUIRED_USE="!esl reduced-ll",
        ),
    ]
