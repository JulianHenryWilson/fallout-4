# Copyright Copyright 2022-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(NexusMod, Fallout4):
    NAME = "Grease Gun SMG"
    DESC = "This mod adds a Grease Gun a.k.a. 9mm Submachine Gun, seen in FO2 and FNV"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/21921"
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout4"
    RDEPEND = "tactical-reload? ( assets-animations/tactical-reload )"
    IUSE = "tactical-reload"
    NEXUS_SRC_URI = """
        https://nexusmods.com/fallout4/mods/21921?tab=files&file_id=90736 -> Grease_Gun_SMG-21921-1-0-4-1.zip
        tactical-reload? ( https://www.nexusmods.com/fallout4/mods/52619?tab=files&file_id=210786 -> Grease_Gun_SMG_-_Tactical_Reload_Patch-52619-1-1-1624264277.rar )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data", S="Grease_Gun_SMG-21921-1-0-4-1", PLUGINS=[File("GreaseGunSMG.esp")]
        ),
        InstallDir(
            ".",
            S="Grease_Gun_SMG_-_Tactical_Reload_Patch-52619-1-1-1624264277",
            PLUGINS=[File("GreaseGunSMG_TacticalReloadPatch.esp")],
            REQUIRED_USE="tactical-reload",
        ),
    ]
